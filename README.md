### Understanding Parser Combinators

A simple exercise in confirming my understanding of parser combinators, by
working through the [Understanding Parser Combinators series by Scott Wlaschin](https://fsharpforfunandprofit.com/posts/understanding-parser-combinators/) and porting the examples to Standard ML.


One notable difference from the original F# examples, is that the parse function
is written here in curried form from the outset (resulting in a different
signature for implementations 2 & 3, and allowing us to omit implementation 4:
'Switching to a curried implementation').  Currying is the default and idiomatic
style in Standard ML.


In the first post in the series, the author's implementation of ```choice```
will fail if the argument list is empty.  We simply avoid this problem by
requiring at least one parser to be supplied with the function signature:

```'a parser -> ('a parser) list -> 'a parser```
