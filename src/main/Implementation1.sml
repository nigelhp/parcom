(* Parses a single hard-coded character, the letter "A" *)
structure Implementation1 = 
struct
  (* string -> bool * string *)
  fun A_parser (str : string) : bool * string =
      case (String.explode str) of
          #"A" :: t => (true, String.implode t)
        | _         => (false, str) 
end;
