(* Parses a specified character - returning a Success / Failure *)
structure Implementation3 = 
struct
  datatype 'a result = SUCCESS of 'a
                     | FAILURE of string
                                    
  (* char -> string -> (char * string) result *)
  fun pchar (c: char) (input : string) : (char * string) result =
      case (String.explode input) of
          []     => FAILURE "No more input"
        | h :: t => if h = c then SUCCESS (c, String.implode t)
                    else FAILURE ("Expecting " ^ str c ^ ". Got " ^ str h)
end;
