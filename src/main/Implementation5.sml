(* Parses a specified character - returning a Success / Failure *)
(* The parsing function is now encapsulated in a type *)
structure Implementation5 = 
struct
  datatype 'a result = SUCCESS of 'a
                     | FAILURE of string

  datatype 'a parser = PARSER of string -> ('a * string) result
                                                         
  (* char -> char parser *)
  fun pchar (c: char) : char parser =
      let
        fun innerFn input =
            case (String.explode input) of
                []     => FAILURE "No more input"
              | h :: t => if h = c then SUCCESS (c, String.implode t)
                          else FAILURE ("Expecting " ^ str c ^ ". Got " ^ str h)
      in
        PARSER innerFn
      end
          
  (* 'a parser -> string -> ('a * string) result *)
  fun run parser input =
      (* unwrap parser to get inner function *)
      let
        val (PARSER innerFn) = parser
      in
        innerFn input
      end

  (* 'a parser -> 'b parser -> ('a * 'b) parser *)
  fun andThen parser1 parser2 =
      let
        fun innerFn input =
            case (run parser1 input) of
                FAILURE err                  => FAILURE err
              | SUCCESS (value1, remaining1) =>
                case (run parser2 remaining1) of
                    FAILURE err                  => FAILURE err
                  | SUCCESS (value2, remaining2) =>
                    SUCCESS ((value1, value2), remaining2) 
      in
        PARSER innerFn
      end
          
  (* 'a parser -> 'a parser -> 'a parser *)
  fun orElse parser1 parser2 =
      let
        fun innerFn input =
            case (run parser1 input) of
                SUCCESS result => SUCCESS result
              | FAILURE err    => run parser2 input
      in
        PARSER innerFn
      end
          
  (* 'a parser -> ('a parser) list -> 'a parser *)
  fun choice parser otherParsers =
      let
        fun uncurry f (a, b) = f a b
      in
        List.foldl (uncurry orElse) parser otherParsers
      end
          
  (* char -> char list -> char parser *)
  fun anyOf char otherChars =
      choice (pchar char) (List.map pchar otherChars)

  (* ('a -> 'b) -> 'a parser -> 'b parser *)
  fun mapP f parser =
      let
        fun innerFn input =
            case (run parser input) of
                SUCCESS (value, remaining) => SUCCESS (f value, remaining)
              | FAILURE err                => FAILURE err
      in
        PARSER innerFn
      end

  (* return / pure / unit *)          
  fun returnP a =
      PARSER (fn input => SUCCESS (a, input))

  (* ('a -> 'b) parser -> 'a parser -> 'b parser *)             
  fun applyP fP aP =
      mapP (fn (f, a) => f a) (andThen fP aP)

  (* ('a -> 'b -> 'c) -> 'a parser -> 'b parser -> 'c parser *)
  fun lift2 f aP bP =
      applyP (applyP (returnP f) aP) bP

  (* ('a parser) list -> ('a list) parser *)             
  fun sequence parserList =
      let
        fun curry f a b = f (a, b)
        val consP = lift2 (curry (op ::))
      in
        case parserList of
            []     => returnP []
          | h :: t => consP h (sequence t) 
      end

  (* string -> string parser *)
  fun pstring s =
      let
        val parseListOfChars = sequence (List.map pchar (explode s))
      in
        mapP implode parseListOfChars
      end

  (* 'a parser -> string -> 'a list * string *)
  fun parseZeroOrMore parser input =
      case (run parser input) of
          FAILURE err                                => ([], input)
        | SUCCESS (firstValue, inputAfterFirstParse) =>
          let
            val (subsequentValues, remainingInput) = parseZeroOrMore parser inputAfterFirstParse
          in
            (firstValue :: subsequentValues, remainingInput)
          end

  (* 'a parser => ('a list) parser *)            
  fun many parser =
      PARSER (fn input => SUCCESS (parseZeroOrMore parser input))

  (* 'a parser => ('a list) parser *)            
  fun many1 parser =
      let
        fun innerFn input =
            case (run parser input) of
                FAILURE err                                => FAILURE err
              | SUCCESS (firstValue, inputAfterFirstParse) =>
                let
                  val (subsequentValues, remainingInput) = parseZeroOrMore parser inputAfterFirstParse
                in
                  SUCCESS (firstValue :: subsequentValues, remainingInput)
                end
      in
        PARSER innerFn
      end

  (* 'a parser -> ('a option) parser *)
  fun opt parser =
      let
        val some = mapP SOME parser
        val none = returnP NONE
      in
        orElse some none
      end
          
  (* unit -> int parser *) 
  fun pint () =
      let
        fun resultToInt (sign, digitList) =
            case (sign, Int.fromString (implode digitList)) of
                (NONE, SOME n)   => n
              | (SOME _, SOME n) => ~n
              | (_, NONE)        => raise Overflow 
        val digit = anyOf #"0" (explode "123456789")
        val digits = many1 digit
        val signedDigits = andThen (opt (pchar #"-")) digits
      in
        mapP resultToInt signedDigits
      end

  (* 'a parser -> 'b parser -> 'b parser *)
  fun dropLeft p1 p2 =
      mapP (fn (a,b) => b) (andThen p1 p2)

  (* 'a parser -> 'b parser -> 'a parser *)
  fun dropRight p1 p2 =
      mapP (fn (a,b) => a) (andThen p1 p2)

  (* 'a parser -> 'b parser -> 'c parser -> 'b parser *)
  fun between p1 p2 p3 =
      dropRight (dropLeft p1 p2) p3

  (* 'a parser -> 'b parser -> ('a list) parser *)
  fun sepBy1 p sep =
      let
        val psep = andThen p (many (dropLeft sep p))
      in
        mapP (fn (x,xs) => x :: xs) psep
      end

  (* 'a parser -> 'b parser -> ('a list) parser *)
  fun sepBy p sep =
      orElse (sepBy1 p sep) (returnP [])

  (* ('a -> 'b parser) -> 'a parser -> 'b parser *)
  fun bindP f p =
      let
        fun innerFn input =
            case (run p input) of
                FAILURE err                     => FAILURE err
              | SUCCESS (value, remainingInput) =>
                run (f value) remainingInput
      in
        PARSER innerFn
      end
          
end;
