(* Parses a specified character *)
structure Implementation2 = 
struct
  (* char -> string -> string * string *)
  fun pchar (c: char) (input : string) : string * string =
      case (String.explode input) of
          []     => ("No more input", "")
        | h :: t => if h = c then ("Found " ^ str c, String.implode t)
                    else ("Expecting " ^ str c ^ ". Got " ^ str h, input)
end;
