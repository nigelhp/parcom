use "../main/Implementation1.sml";

(* good input *)
Implementation1.A_parser "ABC";
(* result is: (true, "BC") *)


(* bad input *)
Implementation1.A_parser "ZBC";
(* result is: (false, "ZBC") *)


(* empty input *)
Implementation1.A_parser "";
(* result is (false, "") *)
