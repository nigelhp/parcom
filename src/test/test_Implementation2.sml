use "../main/Implementation2.sml";

(* good input *)
Implementation2.pchar #"A" "ABC";
(* result is: ("Found A", "BC") *)


(* bad input *)
Implementation2.pchar #"A" "ZBC";
(* result is: ("Expecting A. Got Z", "ZBC") *)


(* empty input *)
Implementation2.pchar #"A" "";
(* result is ("No more input", "") *)
