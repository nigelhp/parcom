use "../main/Implementation5.sml";

open Implementation5;

(* Note that infix directives issued inside a structure have no effect outside *)
(* We therefore create the required infix operators at the top level *)
(* We use :>>: rather than the .>>. of the blogPost for andThen *)
infix :>>:;
fun (parser1 :>>: parser2) = andThen parser1 parser2;

infix <|>;
fun (parser1 <|> parser2) = orElse parser1 parser2;

infix :>>;
fun (parser1 :>> parser2) = dropRight parser1 parser2;


val parseLowercase = anyOf #"a" (List.tabulate (25, (fn n => chr (n + 98))));
val parseDigit = anyOf #"0" (List.tabulate (9, (fn n => chr (n + 49))));
val parseThreeDigits = parseDigit :>>: parseDigit :>>: parseDigit;
val whitespaceChar = anyOf #" " [#"\t", #"\n"];
                           
                                  
let
  val parseA = pchar #"A"
  (* good input, bad input, empty input *)
  val input_examples = ["ABC", "ZBC", ""]
in
  List.map (run parseA) input_examples
  (* result is: [SUCCESS (#"A", "BC"), 
                 FAILURE "Expecting A. Got Z", 
                 FAILURE "No more input"]
  *)
end;

let
  val parseAThenB = (pchar #"A") :>>: (pchar #"B")
  val input_examples = ["ABC", "ZBC", "AZC", "", "A"]                         
in
  List.map (run parseAThenB) input_examples
  (* result is: [SUCCESS ((#"A", #"B"), "C"), 
                 FAILURE "Expecting A. Got Z",
                 FAILURE "Expecting B. Got Z", 
                 FAILURE "No more input",
                 FAILURE "No more input"]
  *)
end;

let
  val parseAOrElseB = (pchar #"A") <|> (pchar #"B")
  val input_examples = ["AZZ", "BZZ", "CZZ", ""]
in
  List.map (run parseAOrElseB) input_examples
  (* result is: [SUCCESS (#"A", "ZZ"), 
                 SUCCESS (#"B", "ZZ"), 
                 FAILURE "Expecting B. Got C",
                 FAILURE "No more input"]
  *)
end;

let
  val bOrElseC = (pchar #"B") <|> (pchar #"C")
  val aAndThenBOrC = (pchar #"A") :>>: bOrElseC
  val input_examples = ["ABZ", "ACZ", "QBZ", "AQZ", "", "A"]
in
  List.map (run aAndThenBOrC) input_examples
  (* result is: [SUCCESS ((#"A", #"B"), "Z"), 
                 SUCCESS ((#"A", #"C"), "Z"),
                 FAILURE "Expecting A. Got Q", 
                 FAILURE "Expecting C. Got Q",
                 FAILURE "No more input", 
                 FAILURE "No more input"]
  *)
end;

let
  val input_examples = ["aBC", "zBC", "ABC", ""]
in
  List.map (run parseLowercase) input_examples
  (* result is: [SUCCESS (#"a", "BC"), 
                 SUCCESS (#"z", "BC"), 
                 FAILURE "Expecting a. Got A",
                 FAILURE "No more input"]
  *)
end;

let
  val input_examples = ["1ABC", "9ABC", "|ABC", ""]
in
  List.map (run parseDigit) input_examples
  (* result is: [SUCCESS (#"1", "ABC"), 
                 SUCCESS (#"9", "ABC"),
                 FAILURE "Expecting 0. Got |", 
                 FAILURE "No more input"]
  *)
end;

let
  fun transformTuple ((c1, c2), c3) = implode [c1, c2, c3]
  fun stringToInt s =
      case (Int.fromString s) of
          SOME n => n
        | NONE   => raise Overflow
  val parseThreeDigitsAsStr = mapP transformTuple parseThreeDigits
  val parseThreeDigitsAsInt = mapP stringToInt parseThreeDigitsAsStr
  val input = "123A"
in
  (run parseThreeDigitsAsStr input,
   run parseThreeDigitsAsInt input)
  (* result is: (SUCCESS ("123", "A"), 
                 SUCCESS (123, "A")) 
  *)
end;

let
  (* We cannot simply reference Int.+ as we require a curried function *) 
  fun plus x y = x + y
  val addP = lift2 plus
in
  run (addP (returnP 2) (returnP 42)) "ABC"
  (* result is: SUCCESS (44, "ABC") *)
end;

let
  val parseFiveLowercase = parseLowercase :>>: parseLowercase :>>: parseLowercase :>>:
                                          parseLowercase :>>: parseLowercase
  fun transformTuple ((((c1, c2), c3), c4), c5) = implode [c1, c2, c3, c4, c5]                         
  val parseFiveLowercaseAsStr = mapP transformTuple parseFiveLowercase
  val startsWithP = lift2 String.isPrefix
  val input_examples = ["helloWorld", "helpsWorld"]
in
  List.map (run (startsWithP (returnP "hell") parseFiveLowercaseAsStr)) input_examples
  (* result is: [SUCCESS (true, "World"), 
                 SUCCESS (false, "World")]
  *)
end;

let
  val parser = sequence [pchar #"A", pchar #"B", pchar #"C"]
  val input_examples = ["ABCD", "ABZD", "ZBCD"]
in
  List.map (run parser) input_examples
  (* result is: [SUCCESS ([#"A", #"B", #"C"], "D"), 
                 FAILURE "Expecting C. Got Z",
                 FAILURE "Expecting A. Got Z"]
  *)
end;

let
  val parseABC = pstring "ABC"
  val input_examples = ["ABCDE", "A|CDE", "AB|DE", ""]
in
  List.map (run parseABC) input_examples
  (* result is: [SUCCESS ("ABC", "DE"), 
                 FAILURE "Expecting B. Got |",
                 FAILURE "Expecting C. Got |", 
                 FAILURE "No more input"]
  *)
end;

let
  val manyA = many (pchar #"A")
  val input_examples = ["ABCD", "AACD", "AAAD", "|BCD"]
in
  List.map (run manyA) input_examples
  (* result is: [SUCCESS ([#"A"], "BCD"), 
                 SUCCESS ([#"A", #"A"], "CD"),
                 SUCCESS ([#"A", #"A", #"A"], "D"), 
                 SUCCESS ([], "|BCD")]
  *)
end;

let
  val manyAB = many (pstring "AB")
  val input_examples = ["ABCD", "ABABCD", "ZCD", "AZCD"]
in
  List.map (run manyAB) input_examples
  (* [SUCCESS (["AB"], "CD"), 
      SUCCESS (["AB", "AB"], "CD"),
      SUCCESS ([], "ZCD"), 
      SUCCESS ([], "AZCD")]
  *)           
end;

let
  val whitespace = many whitespaceChar
  val input_examples = ["ABC", " ABC", "\tABC", "\nABC", " \t \n ABC"]
in
  List.map (run whitespace) input_examples
  (* result is: [SUCCESS ([], "ABC"), 
                 SUCCESS ([#" "], "ABC"), 
                 SUCCESS ([#"\t"], "ABC"),
                 SUCCESS ([#"\n"], "ABC"),
                 SUCCESS ([#" ", #"\t", #" ", #"\n", #" "], "ABC")]
  *)
end;

let
  val digits = many1 parseDigit
  val input_examples = ["1ABC", "12BC", "123C", "1234", "ABC", ""]
in
  List.map (run digits) input_examples
  (* result is: [SUCCESS ([#"1"], "ABC"), 
                 SUCCESS ([#"1", #"2"], "BC"),
                 SUCCESS ([#"1", #"2", #"3"], "C"),
                 SUCCESS ([#"1", #"2", #"3", #"4"], ""), 
                 FAILURE "Expecting 0. Got A",
                 FAILURE "No more input"]
  *)
end;

let
  val input_examples = ["1ABC", "12BC", "123C", "1234", "0123", "ABC", "", "-123C"]
in
  List.map (run (pint ())) input_examples
  (* result is: [SUCCESS (1, "ABC"), 
                 SUCCESS (12, "BC"), 
                 SUCCESS (123, "C"),
                 SUCCESS (1234, ""), 
                 SUCCESS (123, ""), 
                 FAILURE "Expecting 0. Got A",
                 FAILURE "No more input",
                 SUCCESS (~123, "C")]
  *)
end;

let
  val digitThenSemicolon = parseDigit :>>: opt (pchar #";")
  val input_examples = ["1;", "1"]
in
  List.map (run digitThenSemicolon) input_examples
  (* result is: [SUCCESS ((#"1", SOME #";"), ""), 
                 SUCCESS ((#"1", NONE), "")] 
  *)
end;

let
  val digitThenSemicolon = parseDigit :>> opt (pchar #";")
  val input_examples = ["1;", "1"]
in
  List.map (run digitThenSemicolon) input_examples
  (* result is: [SUCCESS (#"1", ""), 
                 SUCCESS (#"1", "")] 
  *)
end;

let
  val whitespace = many1 whitespaceChar
  val ab_cd = ((pstring "AB") :>> whitespace) :>>: (pstring "CD")
  val input_examples = ["AB CD", "AB \t\nCD"]
in
  List.map (run ab_cd) input_examples
  (* result is: [SUCCESS (("AB", "CD"), ""), 
                 SUCCESS (("AB", "CD"), "")]
  *)
end;

let
  val doubleQuote = pchar #"\""
  val quotedInteger = between doubleQuote (pint ()) doubleQuote
in
  run quotedInteger ("\"" ^ "1234" ^ "\"")
  (* result is: SUCCESS (1234, "") *)
end;

let
  val oneOrMoreDigitList = sepBy1 parseDigit (pchar #",")
  val input_examples = ["1;", "1,2;", "1,2,3;", "Z;"]
in
  List.map (run oneOrMoreDigitList) input_examples
  (* result is: [SUCCESS ([#"1"], ";"), 
                 SUCCESS ([#"1", #"2"], ";"),
                 SUCCESS ([#"1", #"2", #"3"], ";"), 
                 FAILURE "Expecting 0. Got Z"]
  *)
end;
  
let
  val zeroOrMoreDigitList = sepBy parseDigit (pchar #",")
  val input_examples = ["1;", "1,2;", "1,2,3;", "Z;"]
in
  List.map (run zeroOrMoreDigitList) input_examples
  (* result is: [SUCCESS ([#"1"], ";"), 
                 SUCCESS ([#"1", #"2"], ";"),
                 SUCCESS ([#"1", #"2", #"3"], ";"), 
                 SUCCESS ([], "Z;")]
  *)
end;
