use "../main/Implementation3.sml";

(* good input *)
Implementation3.pchar #"A" "ABC";
(* result is: SUCCESS (#"A", "BC") *)


(* bad input *)
Implementation3.pchar #"A" "ZBC";
(* result is: FAILURE "Expecting A. Got Z" *)


(* empty input *)
Implementation3.pchar #"A" "";
(* result is FAILURE "No more input" *)
